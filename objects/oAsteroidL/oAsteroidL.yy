{
    "id": "f4c4dbfb-b1fb-4117-8656-b65b354478bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAsteroidL",
    "eventList": [
        {
            "id": "df87349d-d204-4879-8ef6-86717eebacb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f4c4dbfb-b1fb-4117-8656-b65b354478bf"
        },
        {
            "id": "02173d80-1434-4712-9642-51567f1d7c0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f4c4dbfb-b1fb-4117-8656-b65b354478bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1f59be4-d4f2-4e3c-a0bf-a67206336abc",
    "visible": true
}