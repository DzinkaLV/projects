/// @description Insert description here
// You can write your code in this editor

hmove = keyboard_check(vk_right) - keyboard_check(vk_left);
vmove = keyboard_check(vk_down) - keyboard_check(vk_up);
shoot = keyboard_check(vk_space);

//Thrust follow
thrust.x = x;
thrust.y = y;

if (shoot and alarm[0] == -1) {
	alarm[0] = room_speed*recoil;
	
	var s = instance_create_layer(x + 12, y, "Instances", oProtonS);
	s.speed = 10;
}

#region Horizontal Movement

// DISCARD TOO LOW SPEEDS
if (abs(hspd) < 0.05) hspd = 0;

// HARD CAP MAX SPEED
if (abs(hspd) > spd || hmove == 0)
{  
    hspd *= dacc; // the higher this value, the less deceleration
}

// GAIN MOTION
if (abs(hspd) < spd || sign(hspd) != hmove)
{
    hspd += hmove * spd / acc ; // the higher this value, the less acceleration
}

// Move horizontally
x+=hspd;


#endregion

#region Vertical Movement

// DISCARD TOO LOW SPEEDS
if (abs(vspd) < 0.05) vspd = 0;

// HARD CAP MAX SPEED
if (abs(vspd) > spd || vmove == 0)
{  
    vspd *= dacc; // the higher this value, the less deceleration
}

// GAIN MOTION
if (abs(vspd) < spd || sign(vspd) != vmove)
{
    vspd += vmove * spd / acc ; // the higher this value, the less acceleration
}

// Move horizontally
y+=vspd;

#endregion

//Sprite Controll
switch (vmove) {
    case 0:
		thrust.image_speed = .5;
        sprite_index = sPlayer;
		image_speed = 0;
        break;
	case -1:
		thrust.image_speed = 1;
		image_yscale = 1;
		sprite_index = sPlayerTurn;
		if (image_index != 1) image_speed = 1;
		else image_speed = 0;
        break;
	case 1:
		thrust.image_speed = 1;
		image_yscale = -1;
        sprite_index = sPlayerTurn;
		if (image_index != 1) image_speed = 1;
		else image_speed = 0;
        break;
    
}

if (hmove != 0) thrust.image_speed = 1;