{
    "id": "02ba63b0-b3e9-4400-8275-637db33db718",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAsteroidMove",
    "eventList": [
        {
            "id": "2b9e727d-ef8b-4179-b8f0-886cf56f02e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "02ba63b0-b3e9-4400-8275-637db33db718"
        },
        {
            "id": "bef3b3f5-f34b-4683-a406-adc82482d01e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02ba63b0-b3e9-4400-8275-637db33db718"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1f3ed588-4c5f-49be-9526-dd2e5043ccbd",
    "visible": true
}