/// @description Insert description here
// You can write your code in this editor
randomize();

var chance = irandom_range(1, 3);
var number = irandom_range(1, 5);

if (chance == 1) repeat (number) instance_create_depth(x + irandom_range(-16, 16), irandom_range(0, 256), -1, choose(oAsteroidL, oAsteroidMove));

alarm[0] = irandom_range(1, 2)*room_speed;