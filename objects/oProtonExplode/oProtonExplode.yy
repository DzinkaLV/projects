{
    "id": "f49eb143-325e-401c-a6f5-b80d89bf118c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oProtonExplode",
    "eventList": [
        {
            "id": "6a5439d6-4759-419f-b13a-ba4bf9017898",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "f49eb143-325e-401c-a6f5-b80d89bf118c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
    "visible": true
}