{
    "id": "9c7c170a-ff09-4fd3-b489-0a305ec212d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAsteroidS",
    "eventList": [
        {
            "id": "4edd0f5a-abe8-4410-af15-cb18d448f8a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9c7c170a-ff09-4fd3-b489-0a305ec212d9"
        },
        {
            "id": "d47e8eef-c354-4a48-bed5-50bda051e983",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c7c170a-ff09-4fd3-b489-0a305ec212d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e013ef19-7cc3-417e-b3c3-6d944beb1de2",
    "visible": true
}