/// @description Insert description here
// You can write your code in this editor
image_speed = 0;

// Movement
spd = 2;
hspd = 0;
vspd = 0;
dacc = .9; // the higher this value, the less deceleration
acc = 8; // the higher this value, the less acceleration
recoil = .4;

thrust = instance_create_layer(x, y, "Instances", oThrust);