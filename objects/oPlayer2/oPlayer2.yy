{
    "id": "52689a68-4081-487f-94b1-59c54c0bf487",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer2",
    "eventList": [
        {
            "id": "716b6927-d30a-4070-95aa-5d89ee84a68d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52689a68-4081-487f-94b1-59c54c0bf487"
        },
        {
            "id": "3ef92f74-b085-4888-a65f-2476fa46b4d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "52689a68-4081-487f-94b1-59c54c0bf487"
        },
        {
            "id": "ab80ef47-1054-4430-a595-ca0a7d6d9d74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "52689a68-4081-487f-94b1-59c54c0bf487"
        },
        {
            "id": "613d3b6b-bbb7-478c-9652-3ac20b7f3c51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "52689a68-4081-487f-94b1-59c54c0bf487"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "af5ec01f-b541-4d5e-aa21-b395846b27a4",
    "visible": true
}