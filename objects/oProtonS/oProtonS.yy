{
    "id": "88b9985f-aafd-4093-9555-c40d2c86e555",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oProtonS",
    "eventList": [
        {
            "id": "17d8f57d-57d5-47e6-9834-b09fcc3e0373",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "88b9985f-aafd-4093-9555-c40d2c86e555"
        },
        {
            "id": "8f2f1d19-7f33-43d9-a08e-0b73f23c7a03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f4c4dbfb-b1fb-4117-8656-b65b354478bf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "88b9985f-aafd-4093-9555-c40d2c86e555"
        },
        {
            "id": "b0256428-c8bd-42ee-8565-4b9c7da54808",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "975fe75b-8d22-4134-b3b6-593bb92eb064",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "88b9985f-aafd-4093-9555-c40d2c86e555"
        },
        {
            "id": "0c41a679-37a4-4e50-a8c0-56815c693e26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "02ba63b0-b3e9-4400-8275-637db33db718",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "88b9985f-aafd-4093-9555-c40d2c86e555"
        },
        {
            "id": "7dc86f86-3dfb-4476-8875-fa2660445af1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9c7c170a-ff09-4fd3-b489-0a305ec212d9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "88b9985f-aafd-4093-9555-c40d2c86e555"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b27d0639-f4c8-4007-80a6-5a06d8a5c419",
    "visible": true
}