{
    "id": "975fe75b-8d22-4134-b3b6-593bb92eb064",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAsteroidM",
    "eventList": [
        {
            "id": "ec0f7b08-be87-4e1c-b175-b6cb71b12ee0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "975fe75b-8d22-4134-b3b6-593bb92eb064"
        },
        {
            "id": "9a343dbb-4bb9-4717-ae51-c3bd896905e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "975fe75b-8d22-4134-b3b6-593bb92eb064"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dd740464-a817-4f66-813b-851d71f35178",
    "visible": true
}