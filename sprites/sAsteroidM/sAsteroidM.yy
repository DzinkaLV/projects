{
    "id": "1f3ed588-4c5f-49be-9526-dd2e5043ccbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAsteroidM",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "471cbc0d-1640-4d93-bf11-b69c2f173f5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f3ed588-4c5f-49be-9526-dd2e5043ccbd",
            "compositeImage": {
                "id": "cbfdd82c-4807-4195-9e35-51eaf600d4c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "471cbc0d-1640-4d93-bf11-b69c2f173f5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f837226-f19b-4e73-9c06-ded901ec0b1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "471cbc0d-1640-4d93-bf11-b69c2f173f5d",
                    "LayerId": "d4b7d6ad-dfdd-4833-83c8-12cc2c8637a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "d4b7d6ad-dfdd-4833-83c8-12cc2c8637a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f3ed588-4c5f-49be-9526-dd2e5043ccbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 12
}