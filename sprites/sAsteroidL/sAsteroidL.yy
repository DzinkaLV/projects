{
    "id": "e1f59be4-d4f2-4e3c-a0bf-a67206336abc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAsteroidL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01a4f001-361c-4d76-b288-f238efd089ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1f59be4-d4f2-4e3c-a0bf-a67206336abc",
            "compositeImage": {
                "id": "4ea4b3af-ff5a-44f5-85ab-7737ade6ed9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01a4f001-361c-4d76-b288-f238efd089ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1c58aa-b5c5-4ca7-814c-91340802f80a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01a4f001-361c-4d76-b288-f238efd089ba",
                    "LayerId": "c81d2b85-2eab-4b6c-89e6-09ff1350c91b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "c81d2b85-2eab-4b6c-89e6-09ff1350c91b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1f59be4-d4f2-4e3c-a0bf-a67206336abc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 23
}