{
    "id": "84dc234a-e6c1-4afd-987a-4d81ad01c631",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerTurn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "434db5d2-8d27-4d29-b281-44ccbd8c83e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84dc234a-e6c1-4afd-987a-4d81ad01c631",
            "compositeImage": {
                "id": "123c05ae-3077-41af-9882-8884c3fe9bf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "434db5d2-8d27-4d29-b281-44ccbd8c83e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2189d842-1fdb-4a22-acd9-dae99962621a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "434db5d2-8d27-4d29-b281-44ccbd8c83e0",
                    "LayerId": "d3840148-ed03-4ccc-b3d6-d1c44173d6dc"
                }
            ]
        },
        {
            "id": "09beeab9-842c-4b39-b609-bdb9ec341e21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84dc234a-e6c1-4afd-987a-4d81ad01c631",
            "compositeImage": {
                "id": "54c643fd-369c-44ab-8695-2cd32841c3bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09beeab9-842c-4b39-b609-bdb9ec341e21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdf14472-a0ff-4c8a-8502-fb358210ccac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09beeab9-842c-4b39-b609-bdb9ec341e21",
                    "LayerId": "d3840148-ed03-4ccc-b3d6-d1c44173d6dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "d3840148-ed03-4ccc-b3d6-d1c44173d6dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84dc234a-e6c1-4afd-987a-4d81ad01c631",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 12
}