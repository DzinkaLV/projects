{
    "id": "dca68f53-911e-4e47-8ec4-d8d485940cfd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer2Icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c34cf24b-ee95-4c94-83a0-4d558f5f22d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca68f53-911e-4e47-8ec4-d8d485940cfd",
            "compositeImage": {
                "id": "f5988d77-0adc-4259-9df9-6e04db4ebed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c34cf24b-ee95-4c94-83a0-4d558f5f22d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c685049-4786-40f6-ab63-65329aa22401",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c34cf24b-ee95-4c94-83a0-4d558f5f22d9",
                    "LayerId": "890e1a7d-237e-43fc-865e-c289c54444eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "890e1a7d-237e-43fc-865e-c289c54444eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dca68f53-911e-4e47-8ec4-d8d485940cfd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 11
}