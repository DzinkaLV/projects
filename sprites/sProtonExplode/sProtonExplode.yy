{
    "id": "f5485c16-8388-4275-b639-8959f8e7d9ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sProtonExplode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d388d045-8abb-4185-a37c-780d3dfb530d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "compositeImage": {
                "id": "935e06ec-560d-424e-ab75-dce66afd9328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d388d045-8abb-4185-a37c-780d3dfb530d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c783b7-0e3e-436a-a2eb-aa70acee98d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d388d045-8abb-4185-a37c-780d3dfb530d",
                    "LayerId": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3"
                }
            ]
        },
        {
            "id": "4d1524e5-4278-4220-8fd5-a9a5cdcdaef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "compositeImage": {
                "id": "62d2e4bc-8f70-43a3-8561-0730237ba2c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d1524e5-4278-4220-8fd5-a9a5cdcdaef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb10eb9a-c20c-4a03-bc54-691931c4b2c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d1524e5-4278-4220-8fd5-a9a5cdcdaef9",
                    "LayerId": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3"
                }
            ]
        },
        {
            "id": "ec2b1b7e-f530-4201-927c-4f2edb6c9b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "compositeImage": {
                "id": "f964bc63-20a0-4851-859c-42de1dd66778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec2b1b7e-f530-4201-927c-4f2edb6c9b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "860e4b49-ba6b-4ddd-881d-5b66dc3ec96e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec2b1b7e-f530-4201-927c-4f2edb6c9b6e",
                    "LayerId": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3"
                }
            ]
        },
        {
            "id": "109ad0d6-a7e2-47da-990b-e1b327e189bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "compositeImage": {
                "id": "35fd16d5-fb07-4a4e-bfbe-30cc6488e761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109ad0d6-a7e2-47da-990b-e1b327e189bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9ae49f9-ad59-41bd-92f5-4fa370d81d51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109ad0d6-a7e2-47da-990b-e1b327e189bf",
                    "LayerId": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3"
                }
            ]
        },
        {
            "id": "c24bb910-f8df-47c7-9e93-72a911bc8599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "compositeImage": {
                "id": "211584e1-5c85-4d0e-82aa-c07d4409316e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c24bb910-f8df-47c7-9e93-72a911bc8599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efac1579-eedf-4796-b8d5-769dc9d3aaa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c24bb910-f8df-47c7-9e93-72a911bc8599",
                    "LayerId": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3"
                }
            ]
        },
        {
            "id": "54da8c97-c4b8-46a5-b388-d0cca24d74de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "compositeImage": {
                "id": "6b23b8d4-759c-49f9-90bb-7ecce9fc69ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54da8c97-c4b8-46a5-b388-d0cca24d74de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "719c972e-e0d2-43b7-9ce1-fc9a68c7c1bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54da8c97-c4b8-46a5-b388-d0cca24d74de",
                    "LayerId": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3"
                }
            ]
        },
        {
            "id": "3a255c68-8cb7-4a5a-bb2f-e3beaf6b7ea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "compositeImage": {
                "id": "d4c035b7-ae43-4778-9ea7-937cb61fd939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a255c68-8cb7-4a5a-bb2f-e3beaf6b7ea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e3504a-c095-4b67-aa7a-a367e71f85a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a255c68-8cb7-4a5a-bb2f-e3beaf6b7ea8",
                    "LayerId": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3"
                }
            ]
        },
        {
            "id": "f9e1cf14-7e34-49af-a754-a2166b070355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "compositeImage": {
                "id": "a34fcd38-5e25-4680-8057-430a9aed4725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9e1cf14-7e34-49af-a754-a2166b070355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caf443f0-aee7-4b75-816a-fa0abfeb9d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9e1cf14-7e34-49af-a754-a2166b070355",
                    "LayerId": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3"
                }
            ]
        },
        {
            "id": "760d718c-8200-4d09-8d31-9c15e6e3aab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "compositeImage": {
                "id": "814a0d40-4ff0-45ea-b69d-28b94d8ef6fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760d718c-8200-4d09-8d31-9c15e6e3aab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a82e58eb-8b0b-4aa1-93b7-e63972065d11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760d718c-8200-4d09-8d31-9c15e6e3aab9",
                    "LayerId": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "b749a359-4f60-447a-bbdd-8cd4d6ba18d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5485c16-8388-4275-b639-8959f8e7d9ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 13
}