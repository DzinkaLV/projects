{
    "id": "af5ec01f-b541-4d5e-aa21-b395846b27a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f43437a8-02be-4882-b286-9799d49c94a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5ec01f-b541-4d5e-aa21-b395846b27a4",
            "compositeImage": {
                "id": "ed5ef896-a8fc-451f-bc8b-4ed2e6d1b01f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f43437a8-02be-4882-b286-9799d49c94a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97f1f091-64e2-4b2b-a71e-a550df33aca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f43437a8-02be-4882-b286-9799d49c94a0",
                    "LayerId": "680e8fd2-0560-4b2a-863e-e5a72cc1c1da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "680e8fd2-0560-4b2a-863e-e5a72cc1c1da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af5ec01f-b541-4d5e-aa21-b395846b27a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 12
}