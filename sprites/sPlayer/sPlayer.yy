{
    "id": "6d663bfb-7cb9-4da5-812a-2ed8c39e1b29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb1dd521-a3bb-4a27-9813-7c8f78de4fd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d663bfb-7cb9-4da5-812a-2ed8c39e1b29",
            "compositeImage": {
                "id": "6d4beedd-f2ca-48e8-b44f-3547ab664389",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb1dd521-a3bb-4a27-9813-7c8f78de4fd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bb994ab-ee01-4106-a0a4-ed33a359507a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1dd521-a3bb-4a27-9813-7c8f78de4fd6",
                    "LayerId": "35675e3d-4b78-49b9-b7cf-0e23d658d5c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "35675e3d-4b78-49b9-b7cf-0e23d658d5c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d663bfb-7cb9-4da5-812a-2ed8c39e1b29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 12
}