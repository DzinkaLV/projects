{
    "id": "b27d0639-f4c8-4007-80a6-5a06d8a5c419",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sProtonS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49062f28-b4df-4e10-b5a3-714af4e44479",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b27d0639-f4c8-4007-80a6-5a06d8a5c419",
            "compositeImage": {
                "id": "d5d401db-d33c-49c5-a459-128c826997d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49062f28-b4df-4e10-b5a3-714af4e44479",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01d4a059-ce6d-458f-8e5f-eae8d5b6d7c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49062f28-b4df-4e10-b5a3-714af4e44479",
                    "LayerId": "8b2da32d-366e-46f0-b62a-4341f453f119"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "8b2da32d-366e-46f0-b62a-4341f453f119",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b27d0639-f4c8-4007-80a6-5a06d8a5c419",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 3
}