{
    "id": "e013ef19-7cc3-417e-b3c3-6d944beb1de2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAsteroidS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8f32770-3de6-4d52-bc0d-19bdedbb624c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e013ef19-7cc3-417e-b3c3-6d944beb1de2",
            "compositeImage": {
                "id": "43346066-5c33-4eea-b531-f123585e66c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8f32770-3de6-4d52-bc0d-19bdedbb624c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9106ff3-a201-4157-a2c5-e6af9c0b1cf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8f32770-3de6-4d52-bc0d-19bdedbb624c",
                    "LayerId": "88a1234d-eefd-4605-ad0c-b47fe6d0950c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "88a1234d-eefd-4605-ad0c-b47fe6d0950c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e013ef19-7cc3-417e-b3c3-6d944beb1de2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 9
}