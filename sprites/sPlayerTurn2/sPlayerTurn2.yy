{
    "id": "fca2b781-9ce6-4a57-9ada-5598b6f545c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerTurn2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6fd8e7e-555c-46c8-bc91-cb5a5680ff9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fca2b781-9ce6-4a57-9ada-5598b6f545c4",
            "compositeImage": {
                "id": "b31b68b5-a4ab-44cd-ad7b-21fd2f145371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6fd8e7e-555c-46c8-bc91-cb5a5680ff9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961a7ab9-a4a1-4115-930f-dabbb46aacc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6fd8e7e-555c-46c8-bc91-cb5a5680ff9e",
                    "LayerId": "90a10be4-bea5-415a-a289-1900ef292a9c"
                }
            ]
        },
        {
            "id": "9dd650c8-0053-4a71-b974-6007da462eb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fca2b781-9ce6-4a57-9ada-5598b6f545c4",
            "compositeImage": {
                "id": "9e153d3c-23f9-4525-8209-275073dcc086",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dd650c8-0053-4a71-b974-6007da462eb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e21db8-5002-46e2-911f-2977708dc4f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dd650c8-0053-4a71-b974-6007da462eb5",
                    "LayerId": "90a10be4-bea5-415a-a289-1900ef292a9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "90a10be4-bea5-415a-a289-1900ef292a9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fca2b781-9ce6-4a57-9ada-5598b6f545c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 12
}