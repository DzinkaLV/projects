{
    "id": "cb245397-b4d0-43ce-b51b-4c8eabe489c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpace",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e281b000-ea85-4e5f-be5a-6efdb6fbdbdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb245397-b4d0-43ce-b51b-4c8eabe489c1",
            "compositeImage": {
                "id": "b8dc65d1-c67b-4fb1-a13d-1a5868b95e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e281b000-ea85-4e5f-be5a-6efdb6fbdbdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00f909e4-0a2e-43ec-ad0c-17c576cc289c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e281b000-ea85-4e5f-be5a-6efdb6fbdbdb",
                    "LayerId": "543d78f9-b746-43e5-8dd3-9bd6913590fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "543d78f9-b746-43e5-8dd3-9bd6913590fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb245397-b4d0-43ce-b51b-4c8eabe489c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}