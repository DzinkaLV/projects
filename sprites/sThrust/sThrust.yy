{
    "id": "42b3256e-984f-4f4b-8a20-b9b6b8c80f81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sThrust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c5bc8d5-0864-4afb-8957-e1150d5d5064",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42b3256e-984f-4f4b-8a20-b9b6b8c80f81",
            "compositeImage": {
                "id": "2a882718-a702-458d-879a-4f3fafd4b609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c5bc8d5-0864-4afb-8957-e1150d5d5064",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e07f301-be0f-4840-90f4-dad5295cfaff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c5bc8d5-0864-4afb-8957-e1150d5d5064",
                    "LayerId": "5ab7efb6-2f2e-40f0-ab8c-6b387cd3933a"
                }
            ]
        },
        {
            "id": "e17189de-f74a-42b1-aafe-da1a1848faa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42b3256e-984f-4f4b-8a20-b9b6b8c80f81",
            "compositeImage": {
                "id": "4793e456-28ba-48c9-8c21-25b0da18d468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e17189de-f74a-42b1-aafe-da1a1848faa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0b30d7f-b2ee-4903-84dd-516b09bd94c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e17189de-f74a-42b1-aafe-da1a1848faa3",
                    "LayerId": "5ab7efb6-2f2e-40f0-ab8c-6b387cd3933a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "5ab7efb6-2f2e-40f0-ab8c-6b387cd3933a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42b3256e-984f-4f4b-8a20-b9b6b8c80f81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 5,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 18,
    "yorig": 10
}