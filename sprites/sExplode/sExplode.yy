{
    "id": "c68b922d-916a-4c5e-a26d-f04bf31f25a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sExplode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 3,
    "bbox_right": 69,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb493165-dfa3-4a4c-aa9e-9dca3a038ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c68b922d-916a-4c5e-a26d-f04bf31f25a2",
            "compositeImage": {
                "id": "798911d8-ab27-41bd-ac12-c67fc7420a68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb493165-dfa3-4a4c-aa9e-9dca3a038ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c78cbc7d-a1ea-4f76-8912-1f6331bf032a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb493165-dfa3-4a4c-aa9e-9dca3a038ecf",
                    "LayerId": "6272deff-c0d1-42fc-862e-1f623b9cb0ea"
                }
            ]
        },
        {
            "id": "e12f600c-39a3-442e-afb0-3d536744c18b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c68b922d-916a-4c5e-a26d-f04bf31f25a2",
            "compositeImage": {
                "id": "bb7581a2-f7d5-4cf6-a968-dfce800fbd37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e12f600c-39a3-442e-afb0-3d536744c18b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62656f05-4912-496a-82de-2008ce1a70c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e12f600c-39a3-442e-afb0-3d536744c18b",
                    "LayerId": "6272deff-c0d1-42fc-862e-1f623b9cb0ea"
                }
            ]
        },
        {
            "id": "d03becbf-73c8-4c02-886a-4c42a5de75b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c68b922d-916a-4c5e-a26d-f04bf31f25a2",
            "compositeImage": {
                "id": "fa8841b0-38c7-4946-bb7a-9c537c26c383",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d03becbf-73c8-4c02-886a-4c42a5de75b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "521e6b9d-25a9-490d-81a6-eb71fe95f41a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d03becbf-73c8-4c02-886a-4c42a5de75b9",
                    "LayerId": "6272deff-c0d1-42fc-862e-1f623b9cb0ea"
                }
            ]
        },
        {
            "id": "24d355cf-dd4a-4234-b9d7-dfbc844bd190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c68b922d-916a-4c5e-a26d-f04bf31f25a2",
            "compositeImage": {
                "id": "b6dbd182-fef8-4fb6-b612-423eca11acb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24d355cf-dd4a-4234-b9d7-dfbc844bd190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bae8958-6aee-4e11-b00f-e36d418d047a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24d355cf-dd4a-4234-b9d7-dfbc844bd190",
                    "LayerId": "6272deff-c0d1-42fc-862e-1f623b9cb0ea"
                }
            ]
        },
        {
            "id": "aab671c7-73f7-438a-8485-48b98bab6485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c68b922d-916a-4c5e-a26d-f04bf31f25a2",
            "compositeImage": {
                "id": "8f595a8a-8507-4c7f-b46d-5ee1df64079e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab671c7-73f7-438a-8485-48b98bab6485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b212b891-d90f-47c8-8372-232596855be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab671c7-73f7-438a-8485-48b98bab6485",
                    "LayerId": "6272deff-c0d1-42fc-862e-1f623b9cb0ea"
                }
            ]
        },
        {
            "id": "b1820bc9-077b-4493-93ac-e689c45fa188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c68b922d-916a-4c5e-a26d-f04bf31f25a2",
            "compositeImage": {
                "id": "f85512d1-612a-48ef-b1ea-12845ddd361f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1820bc9-077b-4493-93ac-e689c45fa188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c0fd384-ab58-4f1b-81e1-bb82deeb3df7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1820bc9-077b-4493-93ac-e689c45fa188",
                    "LayerId": "6272deff-c0d1-42fc-862e-1f623b9cb0ea"
                }
            ]
        },
        {
            "id": "b3189688-87a8-4d55-9872-9903f3e881e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c68b922d-916a-4c5e-a26d-f04bf31f25a2",
            "compositeImage": {
                "id": "e41c08dd-9f98-4e70-929b-b4be2b600b8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3189688-87a8-4d55-9872-9903f3e881e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2f94258-2c77-4f77-862f-dd4de92b45e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3189688-87a8-4d55-9872-9903f3e881e6",
                    "LayerId": "6272deff-c0d1-42fc-862e-1f623b9cb0ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "6272deff-c0d1-42fc-862e-1f623b9cb0ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c68b922d-916a-4c5e-a26d-f04bf31f25a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 40
}