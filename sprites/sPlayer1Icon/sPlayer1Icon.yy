{
    "id": "713a7667-819b-4b30-9027-ca3738de1929",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer1Icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f43d199-bd6d-411f-a8cb-2755fe739f19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "713a7667-819b-4b30-9027-ca3738de1929",
            "compositeImage": {
                "id": "29b96c85-0e7f-4d19-9608-8dda20e16d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f43d199-bd6d-411f-a8cb-2755fe739f19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4b9fb8f-881b-48c0-b01b-9d5763f69680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f43d199-bd6d-411f-a8cb-2755fe739f19",
                    "LayerId": "6c10c87f-74ca-41df-a1bb-281e6620d399"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "6c10c87f-74ca-41df-a1bb-281e6620d399",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "713a7667-819b-4b30-9027-ca3738de1929",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 11
}