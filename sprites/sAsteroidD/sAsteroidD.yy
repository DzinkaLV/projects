{
    "id": "dd740464-a817-4f66-813b-851d71f35178",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAsteroidD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b99bd9f-a706-4ac3-ab56-f382a11a5004",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd740464-a817-4f66-813b-851d71f35178",
            "compositeImage": {
                "id": "5f614b7d-ae11-4df2-b88d-a34f5fd4671d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b99bd9f-a706-4ac3-ab56-f382a11a5004",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fcc91b2-1108-4223-a0cd-5c27d6c79c72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b99bd9f-a706-4ac3-ab56-f382a11a5004",
                    "LayerId": "b297e993-c4f1-4e32-8fe5-de5e5b509042"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "b297e993-c4f1-4e32-8fe5-de5e5b509042",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd740464-a817-4f66-813b-851d71f35178",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 11
}